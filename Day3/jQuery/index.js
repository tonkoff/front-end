$(document).ready(function () {
    $('#hideImage').click(function () {
        $('img').hide();
    })

    $('#showImage').click(function () {
        $('img').show();
    })

    $('#roundCorners').click(function () {
        $('img').addClass('roundCorners')
    })

    $('#straightCorners').click(function () {
        $('img').removeClass('roundCorners')
    })

    $('#addRedBorder').click(function () {
        $('img').addClass('redBorder')
    })

    $('#removeRedBorder').click(function () {
        $('img').removeClass('redBorder')
    })

    $('#moveUp').click(function () {
        $('img').addClass('moveImageUp')
    })

    $('#moveDown').click(function () {
        $('img').removeClass('moveImageUp')
    })
});



var slideIndex = 1;

showDivs(slideIndex);

function plusDivs(n) {
    showDivs(slideIndex += n);
}

function showDivs(n) {
    var i;
    var x = document.getElementsByClassName("center");
    if (n > x.length) {slideIndex = 1}
    if (n < 1) {slideIndex = x.length}
    for (i = 0; i < x.length; i++) {
        x[i].style.display = "none";

    }
    x[slideIndex-1].style.display = "block";

}