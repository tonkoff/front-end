var orderCount = 0;

function takeOrder(topping, crustType ) {

    orderCount += 1;
    console.log('Order:' + crustType + ' pizza ' + topping );
}

function getSubTotal(itemCount ) {
    return itemCount * 7.50;
}

function getTax() {
     return getSubTotal(orderCount)   * 0.06;
}
function getTotal() {
    return getSubTotal(orderCount) + getTax();
}


takeOrder();
console.log(getSubTotal(orderCount));
console.log(getTotal());