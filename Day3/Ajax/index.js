
function fetchDataUsers() {
    $(document).ready(function () {
        $.ajax({url:"http://jsonplaceholder.typicode.com/users", success: function (json) {
                    for(var i = 0; i < json.length; i++) {
                        var tr = $('<tr/>');
                        $(tr).append("<td>" + json[i].name + "</td>");
                        $(tr).append("<td>" + json[i].username + "</td>");
                        $(tr).append("<td>" + json[i].email + "</td>");
                        $('.table1').append(tr);
                    }
        }})
    })
    // fetch('http://jsonplaceholder.typicode.com/users')
    //     .then(function(response) { return response.json(); })
    //     .then(function(json) {
    //         for(var i = 0; i < json.length; i++) {
    //             var tr = $('<tr/>');
    //             $(tr).append("<td>" + json[i].name + "</td>");
    //             $(tr).append("<td>" + json[i].username + "</td>");
    //             $(tr).append("<td>" + json[i].email + "</td>");
    //             $('.table1').append(tr);
    //
    //         }
    //     });

}

function fetchWeather() {
    $(document).ready(function () {
        $.ajax({url:"http://api.openweathermap.org/data/2.5/weather?q=Plovdiv&units=metric&APPID=01ff3ec8cafa43b40d67a1fccab7c8e2", success: function (json) {
            var tr = $('<tr/>');
            $(tr).append("<td>" + json.name + "</td>");
            $(tr).append("<td>" + json.main.pressure + "</td>");
            $(tr).append("<td>" + json.main.temp + "</td>");
            $(tr).append("<td>" + json.main.humidity + "</td>");
            $(tr).append("<td>" + json.main.temp_min + "</td>");
            $(tr).append("<td>" + json.main.temp_max + "</td>");
            $(tr).append("<td>" + json.wind.speed + "</td>");
            $('.table2').append(tr);
        }})
    })

    // fetch('http://api.openweathermap.org/data/2.5/weather?q=Plovdiv&units=metric&APPID=01ff3ec8cafa43b40d67a1fccab7c8e2')
    //     .then(function(response) { return response.json(); })
    //     .then(function(json) {
    //         var tr = $('<tr/>');
    //         $(tr).append("<td>" + json.name + "</td>");
    //         $(tr).append("<td>" + json.main.pressure + "</td>");
    //         $(tr).append("<td>" + json.main.temp + "</td>");
    //         $(tr).append("<td>" + json.main.humidity + "</td>");
    //         $(tr).append("<td>" + json.main.temp_min + "</td>");
    //         $(tr).append("<td>" + json.main.temp_max + "</td>");
    //         $(tr).append("<td>" + json.wind.speed + "</td>");
    //         $('.table2').append(tr);
    //     });
}