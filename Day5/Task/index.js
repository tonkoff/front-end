
var editor; // use a global for the submit and return data rendering in the examples

$(document).ready(function() {
    editor = new $.fn.dataTable.Editor( {
        ajax: "https://jsonplaceholder.typicode.com/users",
        table: "#mytable",
        idSrc:  'id',
        fields: [ {
            label: "name:",
            name: "name"
        }, {
            label: "username:",
            name: "username"
        }, {
            label: "email:",
            name: "email"
        }
        ]

    } );

   editor.on( 'preSubmit', function ( e, json, data ) {


       var name = this.field( 'name' );
       var username = this.field( 'username' );
       var email = this.field( 'email' );

         if(! name.isMultiValue()) {
             if(!name.val()) {
                 name.error('Enter a name');
             }
             if(name.val().length < 3) {
                 name.error("The name length must be at least 3 chars")
             }
         }

       if(! username.isMultiValue()) {
           if(!username.val()) {
               username.error('Enter a name');
           }
           if(username.val().length < 3) {
               username.error("The name length must be at least 3 chars")
           }
       }

       if(! email.isMultiValue()) {
           if(!email.val()) {
               email.error('Enter a name');
           }
           if(email.val().length < 3) {
               email.error("The name length must be at least 3 chars")
           }
       }

       if(this.inError()) {
             return false;
       }

       $(function(){
           new PNotify({

               title: 'Success',
               text: 'Submitted successfully'

           });
       });


   } );


    $('#mytable').on( 'click', 'tbody td:not(:first-child)', function (e) {
        editor.inline( this, {
            submit: 'allIfChanged'
        } );
    } );

    $('#mytable').DataTable( {
       dom: "Bfrtip",
        ajax: {
             url: 'https://jsonplaceholder.typicode.com/users',
             dataSrc: ''
       },
        columns: [
             {"data": "id"},
            { "data": "name" },
            { "data": "username" },
            { "data": "email" }



        ],
        order: [ 1, 'asc' ],
        select: {
            style:    'os',
            selector: 'td:first-child'
        },
        buttons: [
            { extend: "create", editor: editor },
            { extend: "edit",   editor: editor },
            { extend: "remove", editor: editor }
        ]
    } );
} );